<?php
ini_set('display_errors', 'On');

spl_autoload_register( function ($className) {
    $className = str_replace('\\', DIRECTORY_SEPARATOR, $className).'.php';
    require_once $className;
});

require_once 'vendor/autoload.php';

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

require_once 'application/bootstrap.php';
