<?php
namespace application\controllers;

use application\core\Controller;
use application\models\ArticleModel;
use \Exception;

class ArticleController extends Controller
{
    public function actionIndex()
    {
        $hasTable = ArticleModel::hasTable();
        $articles = $hasTable ? ArticleModel::getAll() : false;
        $this->view->generate('article/index.php', compact('hasTable', 'articles'));
    }

    public function actionCreateTable()
    {
        ArticleModel::createTable();
        header('Location: /article/');
    }

    public function actionDropTable()
    {
        ArticleModel::dropTable();
        header('Location: /article/');
    }

    public function actionNew()
    {
        $this->view->generate('article/new.php');
    }

    public function actionCreate()
    {
        $article = new ArticleModel($_POST['title'], $_POST['text']);
        $article->create();
        header('Location: /article/new/');
    }

    public function actionView($id)
    {
        if (isset($id)) {
            $article = ArticleModel::getById($id);
            $this->view->generate('article/view.php', compact('article'));
        } else {
            header('Location: /article/');
        }
    }

    public function actionEdit($id)
    {
        if (isset($id)) {
            $article = ArticleModel::getById($id);
            $this->view->generate('article/edit.php', compact('article'));
        } else {
            header('Location: /article/');
        }
    }

    public function actionUpdate()
    {
        $article = new ArticleModel($_POST['title'], $_POST['text']);
        $article->setId($_POST['id']);
        $article->update();
        header('Location: /article/edit/' . $article->getId() . '/');
    }

    public function actionRemove()
    {
        ArticleModel::getById($_POST['id'])->delete();
        header('Location: /article/');
    }
}