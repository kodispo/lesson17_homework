<?php
namespace application\controllers;

use application\core\Controller;
use application\models\PortfolioModel;
use \Exception;

class PortfolioController extends Controller
{
    public function actionIndex()
    {
        $hasTable = PortfolioModel::hasTable();
        $portfolios = $hasTable ? PortfolioModel::getAll() : false;
        $this->view->generate('portfolio/index.php', compact('hasTable', 'portfolios'));
    }

    public function actionCreateTable()
    {
        PortfolioModel::createTable();
        header('Location: /portfolio/');
    }

    public function actionDropTable()
    {
        PortfolioModel::dropTable();
        header('Location: /portfolio/');
    }

    public function actionNew()
    {
        $this->view->generate('portfolio/new.php');
    }

    public function actionCreate()
    {
        $portfolio = new PortfolioModel($_POST['title'], $_POST['year'], $_POST['url'], $_POST['description']);
        $portfolio->create();
        header('Location: /portfolio/new/');
    }

    public function actionView($id)
    {
        if (isset($id)) {
            $portfolio = PortfolioModel::getById($id);
            $this->view->generate('portfolio/view.php', compact('portfolio'));
        } else {
            header('Location: /portfolio/');
        }
    }

    public function actionEdit($id)
    {
        if (isset($id)) {
            $portfolio = PortfolioModel::getById($id);
            $this->view->generate('portfolio/edit.php', compact('portfolio'));
        } else {
            header('Location: /portfolio/');
        }
    }

    public function actionUpdate()
    {
        $portfolio = new PortfolioModel($_POST['title'], $_POST['year'], $_POST['url'], $_POST['description']);
        $portfolio->setId($_POST['id']);
        $portfolio->update();
        header('Location: /portfolio/edit/' . $portfolio->getId() . '/');
    }

    public function actionRemove()
    {
        PortfolioModel::getById($_POST['id'])->delete();
        header('Location: /portfolio/');
    }
}