<?php
namespace application\helpers;

class Message
{
    protected $type;
    protected $text;

    public function __construct()
    {
        session_start();
        if (isset($_SESSION['message'])) {
            $this->type = $_SESSION['message']['type'];
            $this->text = $_SESSION['message']['text'];
            unset($_SESSION['message']);
        }
    }

    /**
     * @param string $type 'error' or 'success'
     * @param array $text
    **/
    public static function set($type, $text)
    {
        session_start();
        $_SESSION['message']['type'] = $type;
        $_SESSION['message']['text'] = implode('<br>', $text);
    }

    public function show()
    {
        $html = '';

        if ($this->type && $this->text) :
            $class = ($this->type == 'success') ? 'alert-success' : 'alert-danger';
            ob_start(); ?>

            <div class="alert alert-dismissible fade show mb-3 <?= $class; ?>" role="alert">
                <?= $this->text; ?>
            </div>

            <?php $html = ob_get_contents();
            ob_end_clean();
        endif;

        return $html;
    }
}