<h1 class="mb-3"><?= $article->getTitle(); ?></h1>
<p><?= $article->getText(); ?></p>
<div class="my-3">
    <a href="/article/" class="btn btn-outline-primary btn-sm">Back</a>
</div>