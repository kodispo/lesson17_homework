<?php
use application\helpers\Message;
$message = new Message();
?>

<h1 class="mb-3">Add new article</h1>

<?= $message->show(); ?>

<form action="/article/create/" method="post">
    <div class="form-group row">
        <label for="title" class="col-md-2 col-form-label">Title<span class="text-danger">*</span></label>
        <div class="col-md-10">
            <input type="text" id="title" name="title" placeholder="Enter title" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="text" class="col-md-2 col-form-label">Text</label>
        <div class="col-md-10">
            <textarea id="text" name="text" rows="4" placeholder="Enter text" class="form-control"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="offset-md-2 col-md-10">
            <button type="submit" class="btn btn-outline-primary">Add article</button>
        </div>
    </div>
</form>

<div class="my-3">
    <a href="/article/" class="btn btn-outline-primary btn-sm">Back</a>
</div>