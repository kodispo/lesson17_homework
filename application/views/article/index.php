<h1 class="mb-3">Articles</h1>

<?php if (!$hasTable) : ?>
    <p>Table does not exist yet. <a href="/article/create-table/">Create and populate "articles" table</a></p>
<?php else : ?>
    <p>You may <a href="/article/drop-table/" class="text-danger">drop "articles" table</a> for testing purposes.</p>

    <div class="table-responsive mb-4">
        <table class="table mb-0">
            <thead>
            <th>ID</th>
            <th>Title</th>
            <th>Text</th>
            <th></th>
            </thead>
            <tbody>
            <?php if (!empty($articles)) : ?>
                <?php foreach ($articles as $article) : ?>
                    <tr>
                        <td><?= $article->getId(); ?></td>
                        <td><?= $article->getTitle(); ?></td>
                        <td><?= substr($article->getText(), 0, 20); ?>...</td>
                        <td>
                            <div class="d-flex justify-content-end">
                                <a href="/article/view/<?= $article->getId(); ?>/" class="btn btn-outline-primary btn-sm">View</a>
                                <a href="/article/edit/<?= $article->getId(); ?>/" class="btn btn-outline-primary btn-sm mx-1">Edit</a>
                                <form action="/article/remove/" method="post">
                                    <input type="hidden" name="id" value="<?= $article->getId(); ?>">
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="6">
                        <span class="text-danger">Empty</span>
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td colspan="6" class="text-right">
                    <a href="/article/new/" class="btn btn-outline-primary btn-sm">Add article</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php endif; ?>