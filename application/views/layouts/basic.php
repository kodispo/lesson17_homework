<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lessons 17-18 homework</title>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
        }
        .table th,
        .table td {
            font-size: 15px;
            padding: 5px;
            vertical-align: middle;
        }
    </style>
</head>
<body>
    <div class="container my-5">
        <div class="row">
            <div class="col">
                <ul class="list-group list-group-horizontal">
                    <li class="list-group-item"><a href="/">Home</a></li>
                    <li class="list-group-item"><a href="/home/contacts/">Contacts</a></li>
                    <li class="list-group-item"><a href="/books/">Books</a></li>
                    <li class="list-group-item"><a href="/portfolio/">Portfolio</a></li>
                    <li class="list-group-item"><a href="/article/">Articles</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row">
            <div class="col">
                <?php include 'application/views/'.$view;?>
            </div>
        </div>
    </div>
</body>
</html>