<?php
use application\helpers\Message;
$message = new Message();
?>

<h1 class="mb-3">Add new portfolio</h1>

<?= $message->show(); ?>

<form action="/portfolio/create/" method="post">
    <div class="form-group row">
        <label for="title" class="col-md-2 col-form-label">Title<span class="text-danger">*</span></label>
        <div class="col-md-10">
            <input type="text" id="title" name="title" placeholder="Enter title" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="year" class="col-md-2 col-form-label">Year<span class="text-danger">*</span></label>
        <div class="col-md-10">
            <input type="number" id="year" name="year" placeholder="Enter year" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="url" class="col-md-2 col-form-label">URL<span class="text-danger">*</span></label>
        <div class="col-md-10">
            <input type="text" id="url" name="url" placeholder="Enter URL" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-md-2 col-form-label">Description</label>
        <div class="col-md-10">
            <textarea id="description" name="description" rows="4" placeholder="Enter description" class="form-control"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="offset-md-2 col-md-10">
            <button type="submit" class="btn btn-outline-primary">Add portfolio</button>
        </div>
    </div>
</form>

<div class="my-3">
    <a href="/portfolio/" class="btn btn-outline-primary btn-sm">Back</a>
</div>