<h1 class="mb-3">Portfolio</h1>

<?php if (!$hasTable) : ?>
    <p>Table does not exist yet. <a href="/portfolio/create-table/">Create and populate "portfolio" table</a></p>
<?php else : ?>
    <p>You may <a href="/portfolio/drop-table/" class="text-danger">drop "portfolio" table</a> for testing purposes.</p>

    <div class="table-responsive mb-4">
        <table class="table mb-0">
            <thead>
            <th>ID</th>
            <th>Title</th>
            <th>Year</th>
            <th>URL</th>
            <th>Description</th>
            <th></th>
            </thead>
            <tbody>
            <?php if (!empty($portfolios)) : ?>
                <?php foreach ($portfolios as $portfolio) : ?>
                    <tr>
                        <td><?= $portfolio->getId(); ?></td>
                        <td><?= $portfolio->getTitle(); ?></td>
                        <td><?= $portfolio->getYear(); ?></td>
                        <td><?= $portfolio->getUrl(); ?></td>
                        <td><?= substr($portfolio->getDescription(), 0, 20); ?>...</td>
                        <td>
                            <div class="d-flex justify-content-end">
                                <a href="/portfolio/view/<?= $portfolio->getId(); ?>/" class="btn btn-outline-primary btn-sm">View</a>
                                <a href="/portfolio/edit/<?= $portfolio->getId(); ?>/" class="btn btn-outline-primary btn-sm mx-1">Edit</a>
                                <form action="/portfolio/remove/" method="post">
                                    <input type="hidden" name="id" value="<?= $portfolio->getId(); ?>">
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="6">
                        <span class="text-danger">Empty</span>
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td colspan="6" class="text-right">
                    <a href="/portfolio/new/" class="btn btn-outline-primary btn-sm">Add portfolio</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php endif; ?>