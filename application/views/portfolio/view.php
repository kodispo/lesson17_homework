<h1 class="mb-3"><?= $portfolio->getTitle(); ?></h1>
<ul class="list-group">
    <li class="list-group-item"><strong>Year:</strong> <?= $portfolio->getYear(); ?></li>
    <li class="list-group-item"><strong>URL:</strong> <?= $portfolio->getUrl(); ?></li>
    <li class="list-group-item"><strong>Description:</strong> <?= $portfolio->getDescription(); ?></li>
</ul>
<div class="my-3">
    <a href="/portfolio/" class="btn btn-outline-primary btn-sm">Back</a>
</div>