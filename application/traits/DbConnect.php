<?php
namespace application\traits;

use PDO;

trait DbConnect {

    protected static $db = null;

    protected static function setDb() {
        try {
            $pdo = new PDO(
                'mysql:host=' . $_ENV['DB_HOST'] .
                ';dbname=' . $_ENV['DB_NAME'],
                $_ENV['DB_USER'],
                $_ENV['DB_PASS']
            );
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->exec('SET NAMES "utf8"');
            self::$db = $pdo;
        } catch (Exception $exception) {
            echo '<strong>Failed to connect to database!</strong><br>' . $exception->getMessage();
            die();
        }
    }

    protected static function getDb() {
        if (self::$db === null) {
            self::setDb();
        }
        return self::$db;
    }

}