<?php
namespace application\traits;

trait Validator {

    protected static function isNotEmpty($input)
    {
        return !empty($input);
    }

    protected static function isValidYear($input)
    {
        if ((int) $input > 1901 && (int) $input < 2155) {
            return true;
        }
        return false;
    }

    protected static function isValidUrl($input)
    {
        return filter_var($input, FILTER_VALIDATE_URL);
    }

}