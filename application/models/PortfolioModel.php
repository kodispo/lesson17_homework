<?php
namespace application\models;

use application\core\Model;
use application\helpers\Message;
use PDO;

class PortfolioModel extends Model
{
    protected $id;
    protected $title;
    protected $year;
    protected $url;
    protected $description;

    public function __construct($title = null, $year = null, $url = null, $description = null)
    {
        $this->title = (string) htmlspecialchars($title);
        $this->year = (int) htmlspecialchars($year);
        $this->url = (string) htmlspecialchars($url);
        $this->description = (string) htmlspecialchars($description);
    }


    public static function hasTable()
    {
        try {
            $sql = 'SHOW TABLES LIKE "portfolio"';
            $stmt = self::getDb()->query($sql);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $exception) {
            echo '<strong>Failed to define if table exists!</strong><br>' . $exception->getMessage();
            die();
        }
    }

    public static function createTable()
    {
        try {
            $sql = 'CREATE TABLE portfolio (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                title VARCHAR (255) NOT NULL,
                year YEAR NOT NULL,
                url VARCHAR (255) NOT NULL,
                description TEXT
            ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
            self::getDb()->exec($sql);
            self::populateTable();
        } catch (Exception $exception) {
            echo '<strong>Failed to create table!</strong><br>' . $exception->getMessage();
            die();
        }
    }

    public static function dropTable()
    {
        try {
            self::getDb()->exec("DROP TABLE portfolio");
        } catch (Exception $exception) {
            echo '<strong>Failed to drop table!</strong><br>' . $exception->getMessage();
            die();
        }
    }

    protected static function populateTable()
    {
        try {
            $sql = '
                INSERT INTO portfolio (title, year, url, description)
                VALUES
                    ("Vix rebum propriae", 2004, "https://test1.com", "Sit choro petentium id. His id quas falli laoreet, id nam vitae quaeque. Duo aeque consul accumsan id. Cu exerci nullam liberavisse sea, choro eruditi intellegam id mei."),
                    ("Et nam dicat sonet ocurreret", 2004, "https://test2.com", "Aliquam facilisi duo id, definiebas elaboraret qui an, cu vivendo ancillae nec. Imperdiet rationibus posidonium ei eos, mei detracto constituam ea. Quando fierent id duo, vel semper fastidii qualisque ne, persius tibique ancillae an cum. An clita postulant reprehendunt eos."),
                    ("Viris vivendo qualisque est", 2009, "https://test3.com", "Est no magna propriae nominati, mei minimum insolens no. Mel wisi imperdiet et, id propriae electram vix. Molestie forensibus mnesarchum an mei, erat semper ei mel. Eos ut nonumes similique, postea democritum usu no. No sed soleat accusam neglegentur, eum ad graeci singulis."),
                    ("Quo latine urbanitas ne", 2012, "https://test4.com", "His at unum decore neglegentur, id animal epicuri vel. Eos ut suas quaerendum. Labore detracto concludaturque id vim."),
                    ("Explicari disputationi vis ea", 2013, "https://test5.com", "Brute eloquentiam ei sea, vix cu laboramus consetetur. Vocibus deseruisse sententiae an mel, vero luptatum eu est, ut ludus causae mea. An nec erat appellantur, quo no etiam debitis, has enim petentium persecuti ut. Utroque repudiare per cu."),
                    ("Scripta eligendi inciderint", 2019, "https://test6.com", "Sea an assentior argumentum, ex fierent electram vis. Vim ea case eirmod platonem, et veri iriure laboramus pro, mei ad sint elaboraret theophrastus. Tollit aliquip definiebas ex cum. Sea ea autem aperiri labores.")
            ';
            self::getDb()->exec($sql);
        } catch (Exception $exception) {
            echo '<strong>Failed to populate table!</strong><br>' . $exception->getMessage();
            die();
        }
    }


    public static function getAll()
    {
        try {
            $sql = 'SELECT id, title, year, url, description FROM portfolio';
            $stmt = self::getDb()->query($sql);
            $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, __CLASS__);
            return $stmt->fetchAll();
        } catch (Exception $exception) {
            echo '<strong>Failed to retrieve all portfolios!</strong><br>' . $exception->getMessage();
            die();
        }
    }

    public function create()
    {
        if (!$this->validate()) return false;

        try {
            $sql = 'INSERT INTO portfolio SET
            title = :title,
            year = :year,
            url = :url,
            description = :description';
            $stmt = self::getDb()->prepare($sql);
            $stmt->execute([
                ':title' => $this->title,
                ':year' => $this->year,
                ':url' => $this->url,
                ':description' => $this->description,
            ]);

            Message::set('success', ['Portfolio has been successfully created!']);
        } catch (Exception $exception) {
            echo '<strong>Failed to add new portfolio!</strong><br>' . $exception->getMessage();
            die();
        }
    }

    protected function validate()
    {
        $errors = [];
        if (!self::isNotEmpty($this->title)) $errors[] = 'Title is empty.';
        if (!self::isValidYear($this->year)) $errors[] = 'Year is empty or invalid.';
        if (!self::isValidUrl($this->url)) $errors[] = 'URL is empty or invalid.';

        if (!empty($errors)) {
            Message::set('error', $errors);
            return false;
        } else {
            return true;
        }
    }

    public static function getById($id) {
        $id = (int) htmlspecialchars($id);
        try {
            $sql = '
                SELECT id, title, year, url, description
                FROM portfolio
                WHERE id = :id
            ';
            $stmt = self::getDb()->prepare($sql);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, __CLASS__);
            return $stmt->fetch();
        } catch (Exception $exception) {
            echo '<strong>Failed to retrieve portfolio!</strong><br>' . $exception->getMessage();
            die();
        }
    }

    public function update()
    {
        if ($this->id == false || !$this->validate()) return false;

        try {
            $sql = 'UPDATE portfolio SET
            title = :title,
            year = :year,
            url = :url,
            description = :description
            WHERE id = :id';
            $stmt = self::getDb()->prepare($sql);
            $stmt->execute([
                ':id' => $this->id,
                ':title' => $this->title,
                ':year' => $this->year,
                ':url' => $this->url,
                ':description' => $this->description,
            ]);

            Message::set('success', ['Portfolio has been successfully updated!']);
        } catch (Exception $exception) {
            echo '<strong>Failed to update portfolio!</strong><br>' . $exception->getMessage();
            die();
        }
    }

    public function delete()
    {
        try {
            $sql = 'DELETE FROM portfolio WHERE id = :id';
            $stmt = self::getDb()->prepare($sql);
            $stmt->bindValue(':id', $this->id);
            $stmt->execute();
        } catch (Exception $exception) {
            echo '<strong>Failed to remove portfolio!</strong><br>' . $exception->getMessage();
            die();
        }
    }


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setId($id)
    {
        $this->id = (int) htmlspecialchars($id);
    }
}