<?php


namespace application\core;


class View
{
    protected $layout = 'layouts/basic.php';

    public function generate($view, $data = null, $layout = null){
        if($layout){
            $this->layout = $layout;
        }

        if (isset($data) && is_array($data)) {
            extract($data);
        }

        include 'application/views/'.$this->layout;
    }
}