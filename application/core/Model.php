<?php


namespace application\core;

use application\traits\DbConnect;
use application\traits\Validator;

class Model
{
    use DbConnect;
    use Validator;
}