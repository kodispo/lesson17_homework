<?php


namespace application\core;


class Route
{
    static public function start(){
        $controllerName = 'home';
        $actionName = 'index';
        $id = null;

        $routes = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
        
        if(!empty($routes[0])){
            $controllerName = $routes[0];
        }

        if(!empty($routes[1])){
            if (strpos($routes[1], '-') !== false) {
                $actionParts = explode('-', $routes[1]);
                foreach ($actionParts as $key => $part) {
                    $actionParts[$key] = ucfirst($part);
                }
                $routes[1] = implode('', $actionParts);
            }
            $actionName = $routes[1];
        }

        if(!empty($routes[2])){
            $id = $routes[2];
        }
        
        $controllerName = '\\application\\controllers\\'.ucfirst($controllerName).'Controller';
        $actionName = 'action'.ucfirst($actionName);

        $controller = new $controllerName;
        $controller->$actionName($id);
        
    }
}